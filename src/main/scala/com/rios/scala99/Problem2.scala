package scala.com.rios.scala99

/**
 * Created by gabriel on 25/11/14.
 */
object Problem2 {

  def penultimate[A](list: List[A]) = list.init.last
}
