package scala.com.rios.scala99

/**
 * Created by gabriel on 25/11/14.
 */
object Problem1 {

  def last[A](list: List[A]) = list.last

  def lastWithMatching[A](list: List[A]) = list match {
    case x :: Nil => x
    case _ => throw new IllegalArgumentException("cannot find last element")
  }
}
